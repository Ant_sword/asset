<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacesTable extends Migration {
    public $timestamps=false;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('places', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->string('name', 50);
            $table->smallInteger('capacity')->unsigned();
            $table->tinyInteger('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('places');
    }

}
