<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
//        add user admin /admin
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@user.com',
            'role_id' => 1,
            'password' => bcrypt('admin'),
            'status' => 1,
        ]);
//        add user user1 /user
        DB::table('users')->insert([
            'name' => 'User1',
            'email' => 'user1@user.com',
            'role_id' => 2,
            'password' => bcrypt('user'),
            'status' => 1,
        ]);
        
        //        add deleted user user0 /user
        DB::table('users')->insert([
            'name' => 'User0',
            'email' => 'user0@user.com',
            'role_id' => 2,
            'password' => bcrypt('user'),
            'status' => 0,
        ]);
    }

}
