<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    public $timestamps= false;
    protected $table = 'places';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'capacity',
        'status'
    ];
}
