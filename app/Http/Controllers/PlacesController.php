<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Place;
use Illuminate\Support\Facades\Gate;

class PlacesController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $places = Place::all();
        
        return view('places.index', compact('places'));
    }
    
    public function getplaces() {
        return response()->json(Place::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        if (!Gate::allows('admin-only', auth()->user())) {
            echo 'sorry';
            return redirect(url()->previous())->withErrors('Anda tidak diizinkan masuk');;
        }
        return view('places.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $request->validate([
            'name' => 'required',
            'capacity' => 'required|integer|max:1000'
        ]);
        $place = new Place([
            'name' => $request->get('name'),
            'capacity' => $request->get('capacity'),
            'status' => 1
        ]);
        $place->save();
        return redirect('/places')->with('success', 'Berhasil menambahkan data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $place = Place::find($id);

        return view('places.edit', compact('place'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $request->validate([
            'name' => 'required',
            'capacity' => 'required|integer|max:1000'
        ]);
        $place = Place::find($id);
        $place->name = $request->get('name');
        $place->capacity = $request->get('capacity');
        $place->status = 1;
        $place->save();
        return redirect('/places')->with('success', 'Berhasil mengubah data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $place = Place::find($id);
        $place->delete();
        return redirect('/places')->with('success', 'Tempat telah dihapus');
    }

}
