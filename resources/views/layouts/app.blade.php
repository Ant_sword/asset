<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'My Project') }}</title>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
        <link href="{{ asset('theme/vendor/font-awesome-5/css/fontawesome-all.min.css') }}" rel="stylesheet" media="all">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">


    </head>
    <body>

        <div id="app">
            <nav class="navbar navbar-expand-sm bg-info navbar-dark">
                <div class="container">
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Asset') }}
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!--Center Of Navbar--> 
                        <ul class="navbar-nav mx-auto nav-menu">
                            <li>
                                <a href="{{route('places.index')}}"><i class="fa fa-lg fa-boxes"> </i>  Tempat</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-lg fa-cube"> </i>  Barang</a>
                            </li>
                        </ul>

                        <!--Right Side Of Navbar--> 
                        <ul class="navbar-nav ml-auto">
                            <!--Authentication Links--> 
                            @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                            @endif
                            @else
                            <li class="nav-item dropdown">

                                <a id="navbarDropdown" class="topnav nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <i class="fas fa-user"></i> {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Pengaturan</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>

            <!--        <nav-component :menus="{{
                        json_encode(
                                [
                                    [
                                        'title'=>'Barang',
                                        'url'=>'#'
                                    ],
                                    [
                                        'title'=>'Tempat',
                                        'url'=>route('places.index')
                                    ]
                                ]
                            )
                            }}">
                    </nav-component>-->
            <div v-cloak>

                <div class="v-cloak--inline"> <!-- Parts that will be visible before compiled your HTML -->
                    <div class="text-center">
                        <br>
                        <i class="spinner-grow spinner-grow-sm text-danger"></i>
                        <i class="spinner-grow spinner-grow-sm text-info"></i>
                        <i class="spinner-grow spinner-grow-sm text-success"></i>
                    </div>
                </div>

                <div class="v-cloak--hidden"> <!-- Parts that will be visible After compiled your HTML -->
                    <!-- Rest of the contents -->
                    <main class="content">
                        <br>
                        @if(session()->get('success'))
                        <b-alert show variant="success" dismissible>
                            {{ session()->get('success') }}  
                        </b-alert>
                        @endif
                        @if ($errors->any())
                        <b-alert show variant="danger"  dismissible>
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </b-alert>
                        @endif
                        @yield('content')
                    </main>
                </div>

            </div>

        </div>
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
    </body>
</html>
