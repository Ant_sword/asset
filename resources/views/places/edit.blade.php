@extends('layouts.app')

@section('content')
<div class="card uper">
  <div class="card-header">
    Ubah Tempat
  </div>
  <div class="card-body">
      <form method="post" action="{{ route('places.update',$place->id) }}">
          @csrf
          @method('PATCH')
          <div class="form-group">
              
              <label for="name">Nama Tempat:</label>
              <input type="text" required class="form-control" name="name" max="50" size='50' value="{{$place->name}}"/>
          </div>
          <div class="form-group">
              <label for="price">Kapasitas Max :</label>
              <input type="number" required class="form-control" max="1000" maxlength="1000" min="0" name="capacity" value='{{$place->capacity}}'/>
          </div>
          <button type="submit" class="btn btn-primary">Ubah</button>
      </form>
  </div>
</div>
@endsection