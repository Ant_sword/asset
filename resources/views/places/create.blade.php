@extends('layouts.app')

@section('content')
<div class="card uper">
  <div class="card-header">
   Tambah Tempat
  </div>
  <div class="card-body">

      <form method="post" action="{{ route('places.store') }}">
          <div class="form-group">
              @csrf
              <label for="name">Nama Tempat:</label>
              <input type="text" required class="form-control" name="name" max="50" size='50'/>
          </div>
          <div class="form-group">
              <label for="price">Kapasitas Max :</label>
              <input type="number" required class="form-control" max="1000" maxlength="1000" min="0" name="capacity"/>
          </div>
          <button type="submit" class="btn btn-primary">Tambah</button>
      </form>
  </div>
</div>
@endsection