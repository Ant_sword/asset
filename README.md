## Important
- This aplication using Laravel 5.8

## Installation
Run on your cmd or terminal
```sh
composer install
```
Copy .env.example file to .env on the root folder. Change the database name (DB_DATABASE) to whatever you have, username (DB_USERNAME) and password (DB_PASSWORD) field correspond to your configuration.
Run on your cmd or terminal
```sh
php artisan key:generate
php artisan migrate
php artisan db:seed
```

## Running App
After database seeder, for login you can use: (e-mail/password)
- admin@user.com / admin

